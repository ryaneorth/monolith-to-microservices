#!/bin/bash

echo -e "GET USER\n"

read -p "Enter user id: " user_id

request_endpoint="GET http://localhost:8080/users/$user_id"

echo -e "\nRequest URL:"
echo $request_endpoint

echo -e "\nResponse Body:"
curl -s -H "Content-Type:application/json" -X $request_endpoint | jq .
