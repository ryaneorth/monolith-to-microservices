#!/bin/bash

echo -e "GET USER NOTE\n"

read -p "Enter user id: " user_ID

request_endpoint="GET http://localhost:8080/notes?userId=$user_ID"

echo -e "\nRequest URL:"
echo $request_endpoint

echo -e "\nResponse Body:"
curl -s -H "Content-Type:application/json" -X $request_endpoint | jq .
