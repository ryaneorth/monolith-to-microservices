#!/bin/bash

echo -e "GET ALL USERS"

request_endpoint="GET http://localhost:8080/users"

echo -e "\nRequest URL:"
echo $request_endpoint

echo -e "\nResponse Body:"
curl -s -H "Content-Type:application/json" -X $request_endpoint | jq .
