#!/bin/bash

echo -e "DELETE NOTE\n"

read -p "Enter note id: " note_id

request_endpoint="DELETE http://localhost:8080/notes/$note_id"

echo -e "\nRequest URL:"
echo $request_endpoint

echo -e "\nResponse Body:"
curl -s -H "Content-Type:application/json" -X $request_endpoint | jq .
