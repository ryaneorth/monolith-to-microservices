#!/bin/bash

echo -e "UPDATE NOTE\n"

read -p "Enter note id: " note_id
read -p "Enter note subject: " note_subject
read -p "Enter note: " note

request_endpoint="PUT http://localhost:8080/notes/$note_id"
request_body=$(cat <<EOF
{
  "subject": "$note_subject",
  "note": "$note"
}
EOF
)

echo -e "\nRequest URL:"
echo $request_endpoint

echo -e "\nRequest Body:"
echo $request_body | jq .

echo -e "\nResponse Body:"
curl -s -H "Content-Type:application/json" -X $request_endpoint -d "$request_body" | jq .
