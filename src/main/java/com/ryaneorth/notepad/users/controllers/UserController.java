package com.ryaneorth.notepad.users.controllers;

import com.ryaneorth.notepad.users.entities.User;
import com.ryaneorth.notepad.users.exceptions.NoSuchUserException;
import com.ryaneorth.notepad.users.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public User getUser(@PathVariable("id") final String id) throws NoSuchUserException {
		return userService.getUser(id);
	}

	@RequestMapping(path = "", method = RequestMethod.GET, produces = "application/json")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}
}
