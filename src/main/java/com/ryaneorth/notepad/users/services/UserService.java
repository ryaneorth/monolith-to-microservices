package com.ryaneorth.notepad.users.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ryaneorth.notepad.users.entities.User;
import com.ryaneorth.notepad.users.exceptions.NoSuchUserException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
	private final List<User> users = new ArrayList<>();

	public User getUser(final String id) throws NoSuchUserException {
		Optional<User> user = users.stream()
			.filter(u -> StringUtils.equals(u.getId(), id))
			.findFirst();

		return user.orElseThrow(NoSuchUserException::new);
	}

	public List<User> getAllUsers() {
		return Collections.unmodifiableList(users);
	}

	public void verifyUserExists(final String id) throws NoSuchUserException {
		users.stream()
			.filter(u -> StringUtils.equals(u.getId(), id))
			.findFirst()
			.orElseThrow(NoSuchUserException::new);
	}

	@PostConstruct
	public void populateUsers() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		try(InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("users.json")) {
			users.addAll(mapper.readValue(inputStream, new TypeReference<List<User>>(){}));
		}
	}
}
