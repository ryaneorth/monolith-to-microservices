package com.ryaneorth.notepad.users.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoSuchUserException extends Exception {
	public NoSuchUserException() {
		super("A user with the specified identifier does not exist");
	}
}
