package com.ryaneorth.notepad.notes.controllers;

import com.ryaneorth.notepad.notes.entities.Note;
import com.ryaneorth.notepad.notes.exceptions.NoSuchNoteException;
import com.ryaneorth.notepad.notes.requests.NoteAddRequest;
import com.ryaneorth.notepad.notes.requests.NoteUpdateRequest;
import com.ryaneorth.notepad.notes.services.NoteService;
import com.ryaneorth.notepad.users.exceptions.NoSuchUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/notes")
public class NoteController {

	@Autowired
	private NoteService noteService;

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public Note addNote(@RequestBody final NoteAddRequest addRequest) throws NoSuchUserException {
		return noteService.addNote(addRequest);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, produces = "application/json")
	public Note updateNote(@PathVariable("id") final String id, @RequestBody final NoteUpdateRequest updateRequest) throws NoSuchNoteException {
		return noteService.updateNote(id, updateRequest);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteNote(@PathVariable("id") final String id) throws NoSuchNoteException {
		noteService.deleteNote(id);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public Note getNote(@PathVariable("id") final String id) throws NoSuchNoteException {
		return noteService.getNote(id);
	}

	@RequestMapping(path = "", method = RequestMethod.GET, produces = "application/json")
	public List<Note> getNotes(@RequestParam("userId") final String userId) throws NoSuchUserException {
		return noteService.getNotesForUser(userId);
	}
}
