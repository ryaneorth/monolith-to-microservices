package com.ryaneorth.notepad.notes.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoSuchNoteException extends Exception {
	public NoSuchNoteException() {
		super("A note with the specified identifier does not exist");
	}
}
