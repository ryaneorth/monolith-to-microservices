package com.ryaneorth.notepad.notes.entities;

import com.ryaneorth.notepad.notes.requests.NoteAddRequest;
import com.ryaneorth.notepad.notes.requests.NoteUpdateRequest;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class Note implements Serializable {
	private final String id;
	private final String userId;
	private String subject;
	private String note;
	private LocalDateTime createdDate;
	private LocalDateTime updatedDate;

	public Note(final NoteAddRequest userNoteAddRequest) {
		this.id = UUID.randomUUID().toString();
		this.userId = userNoteAddRequest.getUserId();
		this.subject = userNoteAddRequest.getSubject();
		this.note = userNoteAddRequest.getNote();
		this.createdDate = this.updatedDate = LocalDateTime.now();
	}

	public String getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCreatedDate() {
		return DateTimeFormatter.ISO_DATE_TIME.format(createdDate);
	}

	public String getUpdatedDate() {
		return DateTimeFormatter.ISO_DATE_TIME.format(updatedDate);
	}

	public void update(NoteUpdateRequest updateRequest) {
		this.updatedDate = LocalDateTime.now();
		this.subject = updateRequest.getSubject();
		this.note = updateRequest.getNote();
	}
}
