package com.ryaneorth.notepad.notes.services;

import com.ryaneorth.notepad.notes.entities.Note;
import com.ryaneorth.notepad.notes.exceptions.NoSuchNoteException;
import com.ryaneorth.notepad.notes.requests.NoteAddRequest;
import com.ryaneorth.notepad.notes.requests.NoteUpdateRequest;
import com.ryaneorth.notepad.users.exceptions.NoSuchUserException;
import com.ryaneorth.notepad.users.services.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NoteService {
	private final List<Note> notes = new ArrayList<>();

	@Autowired
	private UserService userService;

	public Note addNote(final NoteAddRequest addRequest) throws NoSuchUserException {
		userService.verifyUserExists(addRequest.getUserId());

		Note note = new Note(addRequest);
		notes.add(note);

		return note;
	}

	public Note updateNote(final String id, final NoteUpdateRequest updateRequest) throws NoSuchNoteException {
		Note note = getNote(id);
		note.update(updateRequest);

		return note;
	}

	public void deleteNote(final String id) throws NoSuchNoteException {
		Note note = getNote(id);
		notes.remove(note);
	}

	public Note getNote(final String id) throws NoSuchNoteException {
		Optional<Note> note = notes.stream()
			.filter(n -> StringUtils.equals(n.getId(), id))
			.findFirst();

		return note.orElseThrow(() -> new NoSuchNoteException());
	}

	public List<Note> getNotesForUser(final String userId) throws NoSuchUserException {
		userService.verifyUserExists(userId);
		return notes.stream()
			.filter(n -> StringUtils.equals(n.getUserId(), userId))
			.collect(Collectors.toList());
	}
}
