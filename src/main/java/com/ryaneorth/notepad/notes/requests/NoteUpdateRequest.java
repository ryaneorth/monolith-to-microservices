package com.ryaneorth.notepad.notes.requests;

import java.io.Serializable;

public class NoteUpdateRequest implements Serializable {
	private String subject;
	private String note;

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getNote() {
		return note;
	}

	public void setNote(final String note) {
		this.note = note;
	}
}
