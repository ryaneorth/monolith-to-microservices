package com.ryaneorth.notepad.notes.requests;

import java.io.Serializable;

public class NoteAddRequest implements Serializable {
	private String userId;
	private String subject;
	private String note;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getNote() {
		return note;
	}

	public void setNote(final String note) {
		this.note = note;
	}
}
