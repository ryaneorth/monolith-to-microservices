# Monolith to Microservices
* Part 1: [The Monolith](https://bitbucket.org/ryaneorth/monolith-to-microservices/src/monolith/README.md)
* Part 2: [Microservices](https://bitbucket.org/ryaneorth/monolith-to-microservices/src/microservices/README.md)

This repository starts with a monolithic application implemented using Spring Boot and breaks it down into microservices using the following technologies from Netflix:

* Eureka
* Zuul
* Hystrix
* Feign clients

## Part 1: The Monolith

There are many advantages to breaking up monolithic architected applications into smaller and more manageable microservices. Rather than discuss these advantages in theory the goal of this repository is to instead illustrate them through concrete code examples.

### Requirements
In order to download the source code for this application and run it locally you will need to ensure the following items are installed:

* [Java 8](http://www.oracle.com/technetwork/java/index.html)
* [Git](https://git-scm.com/downloads)
* [Maven](https://maven.apache.org/download.cgi)
* [jq](https://stedolan.github.io/jq/download/)

### The Application
This simple Notepad application that is implemented in this repository is based on [Spring Boot](https://projects.spring.io/spring-boot/). It has 2 main objects: *Users* and *Notes*. A user can have 0 or more notes associated with them. As the focus of this application is not on databases, there is no backing datastore and all objects are stored in memory.

![Class Diagram](images/appDesign.png)

All users are loaded into the system at startup so the primary focus is CRUD operations on the *Notes* object. As such, the *NoteController* looks as follows:

```
@RestController
@RequestMapping("/notes")
public class NoteController {

	@Autowired
	private NoteService noteService;

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public Note addNote(@RequestBody final NoteAddRequest addRequest) throws NoSuchUserException {
		return noteService.addNote(addRequest);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, produces = "application/json")
	public Note updateNote(@PathVariable("id") final String id, @RequestBody final NoteUpdateRequest updateRequest) throws NoSuchNoteException {
		return noteService.updateNote(id, updateRequest);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteNote(@PathVariable("id") final String id) throws NoSuchNoteException {
		noteService.deleteNote(id);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public Note getNote(@PathVariable("id") final String id) throws NoSuchNoteException {
		return noteService.getNote(id);
	}

	@RequestMapping(path = "", method = RequestMethod.GET, produces = "application/json")
	public List<Note> getNotes(@RequestParam("userId") final String userId) throws NoSuchUserException {
		return noteService.getNotesForUser(userId);
	}
}
```

And here is the *UserController*:

```
@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public User getUser(@PathVariable("id") final String id) throws NoSuchUserException {
		return userService.getUser(id);
	}

	@RequestMapping(path = "", method = RequestMethod.GET, produces = "application/json")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}
}
```

Not surprisingly, all logic for both users and notes is contained within one application as this is a monolithic architecture. When a note is added, the *NoteService* makes a call to the *UserService* to verify the user we are adding the note to exists:

```
	@Autowired
	private UserService userService;

	public Note addNote(final NoteAddRequest addRequest) throws NoSuchUserException {
		userService.verifyUserExists(addRequest.getUserId());

		Note note = new Note(addRequest);
		notes.add(note);

		return note;
	}
```

The application is listening on port 8080 so all REST calls will be made to *http://localhost:8080/users/\** and *http://localhost:8080/notes/\**. 

### Running the application
To start the monolithic application, checkout the [monolith](https://bitbucket.org/ryaneorth/monolith-to-microservices/src/monolith) branch of this repository. Once checked out run the following commands from the command line:

```
mvn clean install
mvn spring-boot:run
``` 

These commands will build and start the application. Once started, open a new terminal window and browse to the *scripts* directory. Here, you will see a number of useful scripts for making REST calls to the application. Start by making a call to *getAllUsers.sh* to see the list of users. Then copy one of the users' *id* fields and execute the *addNote.sh* operation, supplying the copied user *id* when prompted. Play around with these scripts to familiarize yourself with the application.

![Add Note Output](images/addNoteOutput.png)

Once you have explored the code and played around with the application jump to [Part 2](https://bitbucket.org/ryaneorth/monolith-to-microservices/src/microservices/README.md) to learn how to break this application into microservices using Netflix Eureka, Zuul, Hystrix, and Feign clients!
